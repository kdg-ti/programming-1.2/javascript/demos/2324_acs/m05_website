import {checkDateOfBirth} from "./checkDates.js";

const usernameElement = document.getElementById('username');
const dobElement = document.getElementById('dob');
const passwordElement = document.getElementById('password');
const confirmPasswordElement = document.getElementById('confirm_password');
const agreeElement = document.getElementById('agree');

const signupElement = document.getElementById('signup');
signupElement.addEventListener('click', submitForm);

const passwordStrenghClasses = ['pass_very_weak', 'pass_weak', 'pass_ok', 'pass_strong'];

passwordElement.addEventListener('input', updatePasswordStrength);

function submitForm() {
    const errorMessageElementUsername = getAndClearErrorMessageElement(usernameElement);

    let allOk = true;

    if (!usernameElement.value) {
        addErrorMessage(errorMessageElementUsername, 'Please choose a user name.');
        allOk = false;
    }

    if (usernameElement.value && usernameElement.value.length < 4) {
        addErrorMessage(errorMessageElementUsername, 'Username should be at least length 4');
        allOk = false;
    }

    showHideErrorMessages(errorMessageElementUsername);

    const errorMessageElementDob = getAndClearErrorMessageElement(dobElement);

    /**
     * Checks for date of birth.
     */
    try{
        checkDateOfBirth(dobElement.value)
    } catch(error){
        addErrorMessage(errorMessageElementDob, error.message);
    }
    showHideErrorMessages(errorMessageElementDob);

    //run the date validation check!
    // if (dobElement.value === '') {
    //     addErrorMessage(errorMessageElementDob, 'Please provide the date of birth');
    //     allOk = false;
    // } else {
    //     let dateParts = dobElement.value.split('-');
    //     console.log(dobElement.value);
    //     console.log((dateParts));
    //     if (dateParts.length !== 3) {
    //         addErrorMessage(errorMessageElementDob, 'Please provide a valid date YYYY-MM-dd');
    //         allOk = false;
    //     }
    //     let date = dobElement.value;
    //     // if(!date.match(/^[12]\d{3}-1[012]|[1-9]\d]-[0-3]?\d$/)) {
    //     //
    //     // }
    //
    //     // if(dateParts[1].match(/^(1[012]|[1-9])$/)) {
    //     if(dateParts[1].match(/^[0-1]?\d$/)) {
    //         addErrorMessage(errorMessageElementDob, 'Invalid month');
    //     }

        // if(typeof dateParts[0] !== 'number') {
        //    addErrorMessage(errorMessageElementDob, "Not number for year");
        // }
    // }
    showHideErrorMessages(errorMessageElementDob);

    if(allOk) {
        localStorage.setItem("SINGED_IN_USER", usernameElement.value);
        document.forms[0].submit();
    }
}
function getAndClearErrorMessageElement(element) {
    const errorMessageElement = element.parentElement.querySelector('.text-danger');
    errorMessageElement.firstElementChild.textContent = null;
    return errorMessageElement;
}

function addErrorMessage(errorMessageElement, message) {
    if (errorMessageElement.firstElementChild.textContent) {
        errorMessageElement.firstElementChild.textContent += ' ' + message;
    } else {
        errorMessageElement.firstElementChild.textContent = message;
    }
}

function showHideErrorMessages(errorMessageElement) {
    if (errorMessageElement.firstElementChild.textContent) {
        errorMessageElement.style.visibility = 'visible';
    } else {
        errorMessageElement.style.visibility = 'hidden';
    }
}

const passwordStrengthClasses = ['pass_very_weak', 'pass_weak', 'pass_ok', 'pass_strong'];

function updatePasswordStrength() {
    /**
     * @type {String}
     */
    const userPassword = passwordElement.value;

    let strength = 0;

    // Password length
    if (userPassword.length > 5) {
        strength++;
    }

    // Mix of upper case and lower case
    if (userPassword.length > 5 && userPassword.toUpperCase() !== userPassword
        && userPassword.toLowerCase() !== userPassword) {
        strength++;
    }

    // Contains a digit?
    if (userPassword.match(/\d/)) {
        strength++;
    }

    // Punctuation? (just these are fine, no need to list every possible char here...)
    if (userPassword.match(/[.,:;]/)) {
        strength++;
    }

    const buttons = document.querySelectorAll('.strength-meter > button');
    const cssClassForCurrentPassword = passwordStrengthClasses[strength - 1];
    for (let i = 0; i < buttons.length; i++) {
        const button = buttons[i];
        button.classList.remove(...passwordStrengthClasses);
        if (strength > i) {
            button.classList.add(cssClassForCurrentPassword);
        }
    }
}

