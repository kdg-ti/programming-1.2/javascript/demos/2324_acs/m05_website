export function createTagWithText(tag, text) {
  const node = document.createElement(tag);
  node.textContent = text;
  return node
}